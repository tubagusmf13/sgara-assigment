import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryWaterQualityComponent } from './category-water-quality.component';

describe('CategoryWaterQualityComponent', () => {
  let component: CategoryWaterQualityComponent;
  let fixture: ComponentFixture<CategoryWaterQualityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryWaterQualityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryWaterQualityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
