import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPondDiseasComponent } from './category-pond-diseas.component';

describe('CategoryPondDiseasComponent', () => {
  let component: CategoryPondDiseasComponent;
  let fixture: ComponentFixture<CategoryPondDiseasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryPondDiseasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPondDiseasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
