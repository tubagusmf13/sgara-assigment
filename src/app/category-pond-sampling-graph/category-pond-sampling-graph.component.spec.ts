import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPondSamplingGraphComponent } from './category-pond-sampling-graph.component';

describe('CategoryPondSamplingGraphComponent', () => {
  let component: CategoryPondSamplingGraphComponent;
  let fixture: ComponentFixture<CategoryPondSamplingGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryPondSamplingGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPondSamplingGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
