import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSamplingComponent } from './dashboard-sampling.component';

describe('DashboardSamplingComponent', () => {
  let component: DashboardSamplingComponent;
  let fixture: ComponentFixture<DashboardSamplingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardSamplingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSamplingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
