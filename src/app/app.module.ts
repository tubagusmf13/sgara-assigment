import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardSamplingComponent } from './dashboard-sampling/dashboard-sampling.component';
import { CategoryBlockComponent } from './category-block/category-block.component';
import { CategoryPondComponent } from './category-pond/category-pond.component';
import { CategoryPondSamplingComponent } from './category-pond-sampling/category-pond-sampling.component';
import { CategoryPondDiseasComponent } from './category-pond-diseas/category-pond-diseas.component';
import { CategoryPondDeathCountComponent } from './category-pond-death-count/category-pond-death-count.component';
import { CategoryPondSamplingGraphComponent } from './category-pond-sampling-graph/category-pond-sampling-graph.component';
import { CategoryWaterQualityComponent } from './category-water-quality/category-water-quality.component';
import { CategoryPondProductsComponent } from './category-pond-products/category-pond-products.component';
import { MainPageComponent } from './main-page/main-page.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    DashboardSamplingComponent,
    CategoryBlockComponent,
    CategoryPondComponent,
    CategoryPondSamplingComponent,
    CategoryPondDiseasComponent,
    CategoryPondDeathCountComponent,
    CategoryPondSamplingGraphComponent,
    CategoryWaterQualityComponent,
    CategoryPondProductsComponent,
    MainPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
