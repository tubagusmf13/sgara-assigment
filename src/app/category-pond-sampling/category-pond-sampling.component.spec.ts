import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPondSamplingComponent } from './category-pond-sampling.component';

describe('CategoryPondSamplingComponent', () => {
  let component: CategoryPondSamplingComponent;
  let fixture: ComponentFixture<CategoryPondSamplingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryPondSamplingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPondSamplingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
