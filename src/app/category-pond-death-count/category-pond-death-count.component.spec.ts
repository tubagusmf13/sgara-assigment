import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPondDeathCountComponent } from './category-pond-death-count.component';

describe('CategoryPondDeathCountComponent', () => {
  let component: CategoryPondDeathCountComponent;
  let fixture: ComponentFixture<CategoryPondDeathCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryPondDeathCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPondDeathCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
