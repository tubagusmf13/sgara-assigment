import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPondComponent } from './category-pond.component';

describe('CategoryPondComponent', () => {
  let component: CategoryPondComponent;
  let fixture: ComponentFixture<CategoryPondComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryPondComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
