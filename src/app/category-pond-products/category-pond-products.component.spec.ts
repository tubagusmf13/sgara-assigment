import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPondProductsComponent } from './category-pond-products.component';

describe('CategoryPondProductsComponent', () => {
  let component: CategoryPondProductsComponent;
  let fixture: ComponentFixture<CategoryPondProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryPondProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPondProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
